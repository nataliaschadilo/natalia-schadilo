package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<String> Winter = new ArrayList<String>();
        Winter.add("December");
        Winter.add("January");
        Winter.add("February");

        for (String month :
                Winter) {
            System.out.println(month);
        }

        ArrayList<String> Spring = new ArrayList<String>();
        Spring.add("March");
        Spring.add("April");
        Spring.add("May");

        ArrayList<String> Summer = new ArrayList<String>();
        Summer.add("June");
        Summer.add("July");
        Summer.add("August");

        ArrayList<String> Autumn = new ArrayList<String>();
        Autumn.add("September");
        Autumn.add("October");
        Autumn.add("November");

        ArrayList<String> Month = new ArrayList<String>();
        Month.addAll(Winter);
        Month.addAll(Spring);
        Month.addAll(Summer);
        Month.addAll(Autumn);

        System.out.println();

        for (String month :
                Month) {
            System.out.println(month);
        }
    }
}
