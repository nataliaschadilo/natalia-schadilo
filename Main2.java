package com.company;

import java.util.ArrayList;
import java.util.Collections;

public class Main2 {

    public static void main(String[] args) {
        ArrayList<String> boys = new ArrayList<>();
        boys.add("Nazar");
        boys.add("Max");
        boys.add("Oleg");
        boys.add("Yura");
        boys.add("Taras");
        boys.add("Oleg");
        boys.add("Yaroslav");


        ArrayList<String> girls = new ArrayList<>();
        girls.add("Nastya");
        girls.add("Yuliya");
        girls.add("Natalia");
        girls.add("Tania");
        girls.add("Marta");


        ArrayList<String> students = new ArrayList<>();
        students.addAll(boys);
        students.addAll(girls);
        Collections.sort(students);

        System.out.println("All:");
        for (String student :
                students) {
            System.out.println(student);
        }
        System.out.println();

        System.out.println("Boys:");
        for (String boy :
                boys) {
            System.out.println(boy);
        }

        System.out.println("Girls:");
        for (String girl :
                girls) {
            System.out.println(girl);
        }
    }
}
