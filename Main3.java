package com.company;

import java.util.ArrayList;
import java.util.List;

public class Main3 {

    public static void main(String[] args) {
        List<Student> students = new ArrayList<Student>();

        Student student1 = new Student();
        student1._group = "UFE-21";
        student1._course = 2;
        student1._name = "Natalya Shadilo";
        student1._ratings.add(5);
        student1._ratings.add(5);
        student1._ratings.add(4);
        students.add(student1);

        Student student2 = new Student();
        student2._group = "UFE-21";
        student2._course = 2;
        student2._name = "Oleh Goy";
        student2._ratings.add(3);
        student2._ratings.add(3);
        student2._ratings.add(3);
        students.add(student2);

        Student student3 = new Student();
        student3._group = "UFE-21";
        student3._course = 2;
        student3._name = "Oleg Gavrish";
        student3._ratings.add(2);
        student3._ratings.add(3);
        student3._ratings.add(3);
        students.add(student3);

        for (Student student :
                students) {
            double middleRating = student.getMiddleRating();

            if(middleRating >= 3)
                student._course++;
        }

        Student.printStudents(students, 3);
    }
}
