package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;

public class Student {
    String _name;
    String _group;
    int _course;
    ArrayList<Integer> _ratings = new ArrayList<Integer>();

    public Student(){

    }

    public double getMiddleRating(){
        int sum = 0;

        for(int ratingIndex = 0; ratingIndex < _ratings.size(); ratingIndex++){
            sum += _ratings.get(ratingIndex);
        }

        double middleRating = (double)sum / (double)_ratings.size();
        return middleRating;
    }

    public void showInfo(){
        System.out.println("name: " + _name);
        System.out.println("course: " + _course);
        System.out.println("group: " + _group);
        for (Integer rating :
                _ratings) {
            System.out.println("rating: " + rating);
        }
        System.out.print("---------------\n");
    }

    public static void printStudents(List<Student> students, int course){
        for(ListIterator<Student> iter = students.listIterator(); iter.hasNext();){
            Student student = iter.next();

            if(student._course == course)
                student.showInfo();
        }
    }
}
